# node-test-utils

collection of utils for testing

**Shippable**
[![Run Status](https://api.shippable.com/projects/57bad3994d7c050e00837dbb/badge?branch=master)](https://app.shippable.com/projects/57bad3994d7c050e00837dbb)
[![Coverage Badge](https://api.shippable.com/projects/57bad3994d7c050e00837dbb/coverageBadge?branch=master)](https://app.shippable.com/projects/57bad3994d7c050e00837dbb)

**Codacy**
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/082c502e1d7d476cae061da5a51a4f2d)](https://www.codacy.com?utm_source=git@bitbucket.org&amp;utm_medium=referral&amp;utm_content=codefrog/ams&amp;utm_campaign=Badge_Grade)
[![Codacy Badge](https://api.codacy.com/project/badge/Coverage/082c502e1d7d476cae061da5a51a4f2d)](https://www.codacy.com?utm_source=git@bitbucket.org&amp;utm_medium=referral&amp;utm_content=codefrog/ams&amp;utm_campaign=Badge_Coverage)

## Utils for:

* koa
* mongoose
