import * as lib from '../src'

import * as mongooseFixtures from './fixtures/mongoose.fixtures'

before(function () {
  this.lib = lib
  this.mongooseFixtures = mongooseFixtures
})

describe('TestUtils', function () {
  const tests = [
    'mongoose',
    'koa'
  ]

  tests.forEach(test => {
    it(`should have property ${test}`, function () {
      this.lib.should.have.property(test)
    })
  })
});
