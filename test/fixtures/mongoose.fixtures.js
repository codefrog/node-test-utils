import mongoose, {Schema, SchemaType} from 'mongoose'

export class Int8 extends SchemaType {
  constructor (key, options) {
    super(key, options, 'Int8')
  }

  cast (val) {
    var _val = Number(val)
    if (isNaN(_val)) {
      throw new Error('Int8: ' + val + ' is not a number')
    }
    _val = Math.round(_val)
    if (_val < -0x80 || _val > 0x7F) {
      throw new Error('Int8: ' + val +
        ' is outside of the range of valid 8-bit ints')
    }
    return _val
  }
}

Schema.Types.Int8 = Int8

export const dummySchema = new Schema({
  string: String,
  requiredString: {type: String, required: true},
  uniqueString: {type: String, unique: true},
  maxLengthString: {type: String, maxLength: 5},
  minLengthString: {type: String, minLength: 5},

  number: Number,
  requiredNumber: {type: Number, required: true},
  uniqueNumber: {type: Number, unique: true},
  maxLengthNumber: {type: Number, maxLength: 5},
  minLengthNumber: {type: Number, minLength: 5},

  binary: Buffer,

  mixed: Schema.Types.Mixed,
  objectId: Schema.Types.ObjectId,
  requiredObjectId: {type: Schema.Types.ObjectId, required: true},
  uniqueObjectId: {type: Schema.Types.ObjectId, unique: true},

  array: [],
  ofString: [String],
  ofNumber: [Number],
  ofDates: [Date],
  ofBuffer: [Buffer],
  ofBoolean: [Boolean],
  ofMixed: [Schema.Types.Mixed],
  ofObjectId: [Schema.Types.ObjectId],

  nested: {
    stuff: {type: String, lowercase: true, trim: true}
  },

  int8: Schema.Types.Int8
}, {
  timestamps: true,
  toJSON: {virtuals: true},
  toObject: {virtuals: true}
})

export const dummyModel = mongoose.model('DummyModel', dummySchema)
