import * as fixtures from './mongoose.fixtures'
import mongoose, {Schema} from 'mongoose'

describe('TestUtils Fixtures', function () {
  describe('SchemaType Int8', function () {
    const schema = new Schema({test: fixtures.Int8})
    const Model = mongoose.model('FixtureSchemaTypeInt8', schema)
    const document = new Model()

    it('should fail validation with CastError if value not castable to int', function () {
      document.test = 'abc'
      const error = document.validateSync()

      error.should.be.an.Error()
      error.should.have.property('errors')
      error.errors.should.have.property('test')

      error.errors.test.name.should.be.eql('CastError')
      error.errors.test.message.should.be.eql('Cast to Int8 failed for value "abc" at path "test"')
      error.errors.test.reason.message.should.be.eql('Int8: abc is not a number')
    })
  })

  describe('Schema DummySchema', function () {
    it('should be a mongoose schema', function () {
      fixtures.dummySchema.should.be.instanceOf(Schema)
    })
  })

  describe('Model DummyModel', function () {
    it('should be a mongoose model', function () {
      fixtures.dummyModel.should.have.property('base')
      fixtures.dummyModel.base.should.have.property('Mongoose')
      fixtures.dummyModel.should.have.property('model')
      fixtures.dummyModel.should.have.property('schema')
    })
  })
})
